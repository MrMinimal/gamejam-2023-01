extends Node3D

const WALKING_SPEED = 0 # in meters per second
const MAX_BANK_ANGLE = 20 # in degrees

# Called when the node enters the scene tree for the first time.
func _ready():
	$AnimationPlayer.play("Armature|run")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	rotate_towards_player()
	rotate_bank_angle()
	move_forward(delta)

func rotate_towards_player():
	var a = Quaternion(transform.basis.orthonormalized())
	var b = Quaternion(get_node('/root/Main/Hero').transform.basis)
	# Interpolate using spherical-linear interpolation (SLERP).
	var c = a.slerp(b,0.01) # find halfway point between a and b
	# Apply back
	transform.basis = Basis(c)

func rotate_bank_angle():
	var target_pos = get_node('/root/Main/Hero').global_position
	var own_pos = self.global_position
	var diff_pos = own_pos - target_pos
	var dot = self.global_transform.basis.z.normalized().dot(diff_pos.normalized())
	var turn_multiplicator
	if dot > 0:
		turn_multiplicator = 1 - dot
	else:
		turn_multiplicator = -1 + dot
	self.rotation_degrees.z = turn_multiplicator * MAX_BANK_ANGLE

func move_forward(delta):
	self.translate_object_local(Vector3.FORWARD * WALKING_SPEED * delta)
