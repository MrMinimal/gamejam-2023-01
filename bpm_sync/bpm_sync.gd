extends Node

# Signals that can be sent to other nodes
signal new_beat_reached

const BPM = 116
const BARS = 4

const COMPENSATE_FRAMES = 2
const COMPENSATE_HZ = 60.0


var playing = false
var previous_beat = 0

func _ready():
	playing = true
	$Player.play()

func _process(_delta):
	if not playing or not $Player.playing:
		return

	var time = 0.0
	time = $Player.get_playback_position() + AudioServer.get_time_since_last_mix() - AudioServer.get_output_latency() + (1 / COMPENSATE_HZ) * COMPENSATE_FRAMES

	var beat = int(time * BPM / 60.0)
	var current_beat = beat % BARS + 1
	
	if previous_beat != current_beat:
		emit_signal("new_beat_reached", current_beat)
		previous_beat = current_beat


func strsec(secs):
	var s = str(secs)
	if (secs < 10):
		s = "0" + s
	return s
