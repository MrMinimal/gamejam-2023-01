extends Node3D

var allowed_to_jump = false
var already_jumped = false

func _ready():
	$AnimationPlayer.play("testing/hop")
	
	spawn_projectile()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_pressed("jump"):
		if allowed_to_jump == true:
			if already_jumped == false:
				jump()
				already_jumped = true


func _on_bpm_sync_new_beat_reached(new_beat):
	if new_beat == 4:
		allowed_to_jump = true
	else:
		allowed_to_jump = false
		already_jumped = false

func jump():
	self.translate(Vector3(0, 1, 0))


func spawn_projectile():
	var scene = preload("res://nut/nut.tscn")
	var instance = scene.instantiate()
	$NutOrigin.add_child(instance)
